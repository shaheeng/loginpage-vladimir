var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ToDoSchema = new Schema({
	username :{
		type:String,
		required: true
	},
	password :{
		type:String,
		required: true
	}
});


ToDoSchema.pre('save', function (next){
	var todo = this;
	var currentDate = new Date();

	if (!todo.created_at){
		todo.created_at = currentDate; 
	}
	next();

});

module.exports = mongoose.model('Todo', ToDoSchema);